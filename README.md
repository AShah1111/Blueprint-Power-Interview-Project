# Blueprint-Power-Interview-Project
This project predicts hourly NYISO day ahead locational based marginal price (LBMP) using market prices inputs. The system is coded in Python – the packages and dependencies used will be explored further. 

**DEPENDENCIES**
* Python 3.5
* Pandas 0.19.2
* Keras 1.2.2
* Numpy 1.12.0
* scikit-learn 0.18.1
* TensorFlow 1.0 (GPU version recommended)


I recommend using Anaconda to build your virtual environment.

**MODEL**
* The model selected was long short-term memory neural network. 
* The 'lookback' value was 1 timestamp unit.
* The variables used to build the model were 'LBMP', "Marginal Cost Losses" and 'Marginal Cost Congestion'.
* A sequential LSTM model was built with 100 epochs and a batch size of 72.

**FINDINGS**
*  From the visualizations and the results of the Dickey-Fuller Test we can conclude that the time series is stationary.
*  The RMSE on the test data for this model was 11.673. Considering the mean value for LBMP across the whole dataset is 27.648, this is a significantly high error.
*  By looking a plot of the predicted vs actual values for the test data, there is a suggestion that the model under-predicts the values for LBMP.

**ADJUSTMENTS**
*  The 'lookback' value can be adjusted so that the model is not forecasting based on the immeadiately preceding timestamp(1 hour), but is looking back several hours. This may improve the predictive power of the model.
